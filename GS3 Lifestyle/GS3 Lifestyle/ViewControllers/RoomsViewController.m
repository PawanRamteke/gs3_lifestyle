//
//  RoomsViewController.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "RoomsViewController.h"
#import "AppCollectionView.h"
#import "CustomCollectionCell.h"
#import "CoreDataHandler.h"

@interface RoomsViewController ()
@end

@implementation RoomsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor appYellowColor];
    
    [self setupCollectionView];
}

-(void)setupCollectionView
{
    UIScrollView *baseScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 60)];
    [self.view addSubview:baseScroll];
    
    NSArray *arrRooms = [CoreDataHandler getRoomListFromCoreData];
    
    AppCollectionView *collectionView = [[AppCollectionView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 20) DataArray:arrRooms itemSize:CGSizeMake((SCREEN_WIDTH - 20) / 3 - 7, 130) customClass:[CustomCollectionCell class]];

    [baseScroll addSubview:collectionView];

    [collectionView onContentItemChange:^{
        baseScroll.contentSize = CGSizeMake(0, CGRectGetMaxY(collectionView.frame)+10);
    }];
    
    [collectionView onItemSelect:^(NSInteger selectedIndex) {
        
    }];
}

@end
