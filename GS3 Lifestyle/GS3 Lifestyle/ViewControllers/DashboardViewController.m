//
//  DashboardViewController.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "DashboardViewController.h"
#import "HomeViewController.h"
#import "RoomsViewController.h"
#import "AutomationViewController.h"
#import "AddDeviceViewController.h"
#import "SettingsViewController.h"

@interface DashboardViewController ()<UITabBarControllerDelegate>

@end

@implementation DashboardViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Home";
    self.delegate = self;
    [self setButtonsForNavigation];
    self.tabBar.tintColor = [UIColor appYellowColor];
    self.tabBar.barTintColor = [UIColor whiteColor];
    
    HomeViewController *homeVC = [[HomeViewController alloc]init];
    RoomsViewController *roomsVC = [[RoomsViewController alloc]init];
    AutomationViewController *automationVC = [[AutomationViewController alloc]init];
    
    UITabBarItem *home = [[UITabBarItem alloc]initWithTitle:@"Home" image:[UIImage imageNamed:@"ic_home"] tag:0];
    homeVC.tabBarItem = home;
   
    UITabBarItem *room = [[UITabBarItem alloc]initWithTitle:@"Rooms" image:[UIImage imageNamed:@"ic_room"] tag:1];
    roomsVC.tabBarItem = room;
    
    UITabBarItem *automation = [[UITabBarItem alloc]initWithTitle:@"Automation" image:[UIImage imageNamed:@"ic_automation"] tag:2];
    automationVC.tabBarItem = automation;
    self.viewControllers = @[homeVC,roomsVC,automationVC];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    switch ( item.tag) {
        case 0:
            self.title = @"Home";
            break;
            
        case 1:
            self.title = @"Your Rooms";
            break;
            
        case 2:
            self.title = @"Automation";
            
            break;
            
        default:
            break;
    }
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}


- (void)setButtonsForNavigation{
    
    CGRect frameimg1 = CGRectMake(0, 0, 40, 50);
    UIButton *settingBtn=[[UIButton alloc]initWithFrame:frameimg1];
    settingBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [settingBtn setImage:[UIImage imageNamed:@"ic_setting"] forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(btnSettingPressed)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton1=[[UIBarButtonItem alloc]initWithCustomView:settingBtn];
    
    UIButton *plusBtn=[[UIButton alloc]initWithFrame:frameimg1];
    plusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [plusBtn setImage:[UIImage imageNamed:@"ic_plus"] forState:UIControlStateNormal];
    [plusBtn addTarget:self action:@selector(btnAddPressed)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton2=[[UIBarButtonItem alloc]initWithCustomView:plusBtn];
    self.navigationItem.rightBarButtonItems = @[barButton1,barButton2];


}

-(void)btnAddPressed
{
    AddDeviceViewController *addDeviceVC = [[AddDeviceViewController alloc]init];
    [self.navigationController pushViewController:addDeviceVC animated:YES];
}

-(void)btnSettingPressed
{
    SettingsViewController *settingsVC = [[SettingsViewController alloc]init];
    [self.navigationController pushViewController:settingsVC animated:YES];

}
@end
