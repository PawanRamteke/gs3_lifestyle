//
//  HomeViewController.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "HomeViewController.h"
#import "AppCollectionView.h"
#import "HomeCollectionCell.h"
#import "CoreDataHandler.h"
#import "FavouriteAccessoriesCell.h"

@interface HomeViewController ()
{
    UIScrollView *baseScroll;
    UILabel *lblFavoriteAccess;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor appYellowColor];
     [self setupLayout];
}

-(void)setupLayout
{
    baseScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 60)];
    [self.view addSubview:baseScroll];
    
    
    UILabel *lblLightOn = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, 30)];
    lblLightOn.textColor = [UIColor whiteColor];
    lblLightOn.font = [UIFont appRegularFont:18];
    lblLightOn.text = @"4 Lights On";
    [baseScroll addSubview:lblLightOn];
    
    UILabel *lblBrightness = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblLightOn.frame), SCREEN_WIDTH - 20, 30)];
    lblBrightness.textColor = [UIColor whiteColor];
    lblBrightness.font = [UIFont appRegularFont:18];
    lblBrightness.text = @"Brightness at 20%";
    [baseScroll addSubview:lblBrightness];
    
    UILabel *lblFavoriteScene = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblBrightness.frame)+20, SCREEN_WIDTH - 20, 40)];
    lblFavoriteScene.textColor = [UIColor whiteColor];
    lblFavoriteScene.font = [UIFont appRegularFont:18];
    lblFavoriteScene.text = @"Favourite Scenes";
    [baseScroll addSubview:lblFavoriteScene];
    
    NSArray *arrFavoriteScene = [CoreDataHandler getFavouriteSceneListFromCoreData];
    
    AppCollectionView *collectionViewFavSce = [[AppCollectionView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblFavoriteScene.frame), SCREEN_WIDTH - 20, SCREEN_HEIGHT - CGRectGetMaxY(lblFavoriteScene.frame)) DataArray:arrFavoriteScene itemSize:CGSizeMake((SCREEN_WIDTH - 20) / 2 - 7, 70) customClass:[HomeCollectionCell class]];

    [baseScroll addSubview:collectionViewFavSce];
    
    lblFavoriteAccess = [[UILabel alloc]init];
    lblFavoriteAccess.textColor = [UIColor whiteColor];
    lblFavoriteAccess.font = [UIFont appRegularFont:18];
    lblFavoriteAccess.text = @"Favourite Accessories";
    [baseScroll addSubview:lblFavoriteAccess];
    
    [collectionViewFavSce onContentItemChange:^{
        lblFavoriteAccess.frame = CGRectMake(10, CGRectGetMaxY(collectionViewFavSce.frame)+20, SCREEN_WIDTH - 20, 40);
        [self setupAccessoriesCollection];
    }];
    
    [collectionViewFavSce onItemSelect:^(NSInteger selectedIndex) {
        
    }];
    
}

-(void)setupAccessoriesCollection
{
    NSArray *arrFavPoints = [CoreDataHandler getFavouritePointListFromCoreData];
    
    AppCollectionView *collectionViewFavAcc = [[AppCollectionView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblFavoriteAccess.frame), SCREEN_WIDTH - 20, SCREEN_HEIGHT) DataArray:arrFavPoints itemSize:CGSizeMake((SCREEN_WIDTH - 20) / 2 - 7, 100) customClass:[FavouriteAccessoriesCell class]];
    
    [baseScroll addSubview:collectionViewFavAcc];
    
    [collectionViewFavAcc onContentItemChange:^{
        //lblFavoriteAccess.frame = CGRectMake(10, CGRectGetMaxY(collectionViewFavSce.frame)+20, SCREEN_WIDTH - 20, 30);
        baseScroll.contentSize = CGSizeMake(0, CGRectGetMaxY(collectionViewFavAcc.frame)+10);
    }];
    
    [collectionViewFavAcc onItemSelect:^(NSInteger selectedIndex) {
        
    }];
}

@end
