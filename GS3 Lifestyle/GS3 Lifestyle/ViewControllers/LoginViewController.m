//
//  LoginViewController.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "LoginViewController.h"
#import "AppTextField.h"
#import "DashboardViewController.h"
#import "AppDelegate.h"

@interface LoginViewController ()
{
    AppTextField *txtFieldUsername;
    AppTextField *txtFieldPassword;
}
@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor appYellowColor];
    [self setupLayout];
}

-(void)setupLayout
{
    TPKeyboardAvoidingScrollView *baseScrollView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:baseScrollView];
    
    UIView *baseView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMidY(self.view.frame) - 100, SCREEN_WIDTH - 40, 200)];
    [baseScrollView addSubview:baseView];
    
    txtFieldUsername = [[AppTextField alloc]initWithFrame:CGRectMake(0, 10, baseView.frame.size.width, 60) placeholder:@"Username"];
    [baseView addSubview:txtFieldUsername];
    
    txtFieldPassword = [[AppTextField alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(txtFieldUsername.frame)+10, baseView.frame.size.width, 60) placeholder:@"Password"];
    txtFieldPassword.secureTextEntry = YES;
    [baseView addSubview:txtFieldPassword];
    
    UIButton *btnLogin = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(txtFieldPassword.frame)+10, baseView.frame.size.width, 60)];
    btnLogin.backgroundColor = [UIColor whiteColor];
    [btnLogin setTitle:@"LOGIN" forState:UIControlStateNormal];
    btnLogin.titleLabel.font = [UIFont appRegularFont:17];
    btnLogin.layer.cornerRadius = btnLogin.frame.size.height/2;
    btnLogin.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    btnLogin.layer.shadowOpacity = 0.3;
    btnLogin.layer.shadowRadius = 2;
    btnLogin.layer.shadowOffset = CGSizeMake(0, 2);
    btnLogin.backgroundColor = [UIColor darkYellowColor];
    [btnLogin addTarget:self action:@selector(btnLoginClicked) forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:btnLogin];
    
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, baseView.frame.origin.y - 80, SCREEN_WIDTH, 50)];
    lblTitle.font = [UIFont appBoldFont:40];
    lblTitle.text = @"GS3 Lifestyle";
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor whiteColor];
    [baseScrollView addSubview:lblTitle];
    
    txtFieldUsername.text = @"canrpatel";
    txtFieldPassword.text = @"Admin@123";
}

-(void)loginService
{
    NSDictionary *param = @{
                                @"Username": txtFieldUsername.text,
                                @"Password": txtFieldPassword.text
                            };
    [VIEWMANAGER showAcytivityIndicator];
    [REMOTE_API CallPOSTWebServiceWithParam:LOGIN_URL params:param sBlock:^(id responseObject) {
        VIEWMANAGER.currentUser = responseObject;
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate callWebServices];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
         [VIEWMANAGER hideAcytivityIndicator];
        
    }];
}

-(void)btnLoginClicked
{
    if ([self validate]) {
        [self loginService];
    }
}

-(BOOL)validate
{
    if (txtFieldUsername.text.length == 0) {
        [CustomAlertView showAlert:@"" withMessage:@"Please enter username and password"];
        return NO;
    }
    
    if (txtFieldPassword.text.length == 0) {
        [CustomAlertView showAlert:@"" withMessage:@"Please enter username and password"];
        return NO;
    }
    return YES;
}

@end
