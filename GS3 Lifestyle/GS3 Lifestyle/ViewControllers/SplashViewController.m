//
//  SplashViewController.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 02/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "SplashViewController.h"
#import "AppDelegate.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor appYellowColor];
    
    UILabel *lblTitle = [[UILabel alloc]init];
    lblTitle.font = [UIFont appBoldFont:30];
    lblTitle.text = @"GS3 Lifestyle";
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblTitle];
    
    [lblTitle enableAutolayout];
    [lblTitle leadingMargin:0];
    [lblTitle trailingMargin:0];
    [lblTitle centerY];
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate callWebServices];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
