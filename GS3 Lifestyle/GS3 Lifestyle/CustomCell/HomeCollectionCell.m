//
//  HomeCollectionCell.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "HomeCollectionCell.h"

@implementation HomeCollectionCell
@synthesize lblTitle;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor darkYellowColor];
        self.layer.cornerRadius = 5.0;
        
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.image = [[UIImage imageNamed:@"ic_automation"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imgView.tintColor = [UIColor yellowColor];
        [self addSubview:imgView];
        [imgView enableAutolayout];
        [imgView leadingMargin:10];
        [imgView centerY];
        [imgView fixWidth:20];
        [imgView fixHeight:20];
        
        lblTitle = [[UILabel alloc]init];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.font = [UIFont appRegularFont:16];
        lblTitle.text = @"Ground floor All On";
        lblTitle.numberOfLines = 0;
        [self addSubview:lblTitle];
        
        [lblTitle enableAutolayout];
        [lblTitle addToRight:10 ofView:imgView];
        [lblTitle centerY];
        [lblTitle trailingMargin:10];
        
        
    }
    return self;
    
}
@end
