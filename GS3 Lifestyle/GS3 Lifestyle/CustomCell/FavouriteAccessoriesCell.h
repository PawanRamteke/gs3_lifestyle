//
//  FavouriteAccessoriesCell.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteAccessoriesCell : UICollectionViewCell
@property (nonatomic,strong)UILabel *lblTitle;
@property (nonatomic,strong)UILabel *lblMsp;
@end
