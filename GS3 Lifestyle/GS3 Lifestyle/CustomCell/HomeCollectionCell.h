//
//  HomeCollectionCell.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionCell : UICollectionViewCell
@property (nonatomic,strong)UILabel *lblTitle;
@end
