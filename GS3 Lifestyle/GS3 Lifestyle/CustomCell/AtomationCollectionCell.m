//
//  AtomationCollectionCell.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "AtomationCollectionCell.h"

@implementation AtomationCollectionCell
@synthesize lblTitle;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor darkYellowColor];
        self.layer.cornerRadius = 5.0;
        
        lblTitle = [[UILabel alloc]init];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.font = [UIFont appRegularFont:18];
        lblTitle.text = @"Scenes";
        lblTitle.numberOfLines = 0;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lblTitle];
        
        [lblTitle enableAutolayout];
        [lblTitle topMargin:0];
        [lblTitle leadingMargin:0];
        [lblTitle trailingMargin:0];
        [lblTitle bottomMargin:0];
        
        
    }
    return self;
    
}
@end
