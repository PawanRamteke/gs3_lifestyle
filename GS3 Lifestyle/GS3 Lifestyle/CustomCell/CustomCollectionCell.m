//
//  CustomCollectionCell.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "CustomCollectionCell.h"

@implementation CustomCollectionCell
@synthesize lblTitle;
@synthesize lblSubTitle;

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor darkYellowColor];
        self.layer.cornerRadius = 5.0;
        
        
        lblSubTitle = [[UILabel alloc]init];
        lblSubTitle.textColor = [UIColor darkGrayColor];
        lblSubTitle.font = [UIFont appRegularFont:13];
        lblSubTitle.text = @"4 Accessories";
        lblSubTitle.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lblSubTitle];
        
        [lblSubTitle enableAutolayout];
        [lblSubTitle leadingMargin:10];
        [lblSubTitle bottomMargin:10];
        [lblSubTitle trailingMargin:10];
        
        lblTitle = [[UILabel alloc]init];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.font = [UIFont appBoldFont:18];
        lblTitle.text = @"Bharat Bhai's Room";
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.numberOfLines = 0;
        [self addSubview:lblTitle];
        
        [lblTitle enableAutolayout];
        [lblTitle leadingMargin:10];
        [lblTitle topMargin:20];
        [lblTitle trailingMargin:10];
        [lblTitle aboveView:10 toView:lblSubTitle];
        
    }
    return self;
    
}
@end
