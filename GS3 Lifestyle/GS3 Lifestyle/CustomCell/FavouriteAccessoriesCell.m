//
//  FavouriteAccessoriesCell.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "FavouriteAccessoriesCell.h"

@implementation FavouriteAccessoriesCell
@synthesize lblTitle;
@synthesize lblMsp;

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor darkYellowColor];
        self.layer.cornerRadius = 5.0;
        
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.image = [[UIImage imageNamed:@"ic_automation"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imgView.tintColor = [UIColor grayColor];
        [self addSubview:imgView];
        [imgView enableAutolayout];
        [imgView leadingMargin:10];
        [imgView topMargin:10];
        [imgView fixWidth:20];
        [imgView fixHeight:20];
        
        lblTitle = [[UILabel alloc]init];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.font = [UIFont appRegularFont:18];
        lblTitle.text = @"";
        lblTitle.numberOfLines = 0;
        [self addSubview:lblTitle];
        
        [lblTitle enableAutolayout];
        [lblTitle leadingMargin:10];
        [lblTitle belowView:10 toView:imgView];
        [lblTitle trailingMargin:10];
        
        lblMsp = [[UILabel alloc]init];
        lblMsp.textColor = [UIColor blackColor];
        lblMsp.font = [UIFont appRegularFont:16];
        lblMsp.text = @"";
        lblMsp.numberOfLines = 0;
        [self addSubview:lblMsp];
        
        [lblMsp enableAutolayout];
        [lblMsp leadingMargin:10];
        [lblMsp belowView:10 toView:lblTitle];
        [lblMsp trailingMargin:10];
    }
    return self;
    
}

@end
