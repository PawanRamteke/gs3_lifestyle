//
//  CustomAlertView.m
//  TourApp
//
//  Created by IOS_DEV on 23/02/17.
//  Algro Inc. All rights reserved.
//

#import "CustomAlertView.h"
#import "General.h"

@implementation CustomAlertView

+ (void)showAlert:(NSString*)title withMessage:(NSString*)message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [[VIEWMANAGER topMostController] presentViewController:alertController animated:YES completion:nil];
}

+ (void)ShowAlert:(NSString*)title withMessage:(NSString*)msg tapHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler)
        {
            handler();
        }
    }];
    [alertController addAction:ok];
    
    [[VIEWMANAGER topMostController] presentViewController:alertController animated:YES completion:nil];
}

+ (void)ShowAlertWithCancel:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler)
        {
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [[VIEWMANAGER topMostController] presentViewController:alertController animated:YES completion:nil];
}

+ (void)ShowAlertWithYesNo:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler)
        {
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [[VIEWMANAGER topMostController] presentViewController:alertController animated:YES completion:nil];
}

+(void)loginAlertForGuest:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please Login to Continue" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler) {
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Later" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [[VIEWMANAGER topMostController] presentViewController:alertController animated:YES completion:nil];
}
@end
