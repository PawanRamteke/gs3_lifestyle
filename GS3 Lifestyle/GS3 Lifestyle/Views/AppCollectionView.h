//
//  AppCollectionView.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ContentChangeBlock)(void);
typedef void(^ItemSelectBlock)(NSInteger selectedIndex);
@interface AppCollectionView : UIView 

@property (nonatomic,copy)ContentChangeBlock block;
@property (nonatomic,copy)ItemSelectBlock itemSelectBlock;
-(instancetype)initWithFrame:(CGRect)frame DataArray:(NSArray *)array itemSize:(CGSize)itemSize customClass:(id)cellClass;

-(void)onContentItemChange:(ContentChangeBlock)blk;
-(void)onItemSelect:(ItemSelectBlock)blk;
@end

