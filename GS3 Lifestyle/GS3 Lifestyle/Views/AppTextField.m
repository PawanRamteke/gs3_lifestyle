//
//  AppTextField.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "AppTextField.h"

@implementation AppTextField

-(instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = YES;
        self.placeholder = placeholder;
        self.leftViewMode = UITextFieldViewModeAlways;
        self.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, self.frame.size.height)];
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        self.textColor = [UIColor whiteColor];

    }
    return self;
}

@end
