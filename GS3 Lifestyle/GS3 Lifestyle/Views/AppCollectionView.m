//
//  AppCollectionView.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 01/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "AppCollectionView.h"
#import "CustomCollectionCell.h"
#import "HomeCollectionCell.h"
#import "AtomationCollectionCell.h"
#import "FavouriteAccessoriesCell.h"

@interface AppCollectionView()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray *arrData;
    UICollectionView *_collectionView;
    CGSize cellSize;
}
@end

@implementation AppCollectionView


-(instancetype)initWithFrame:(CGRect)frame DataArray:(NSArray *)array itemSize:(CGSize)itemSize customClass:(id)cellClass
{
    self = [super initWithFrame:frame];
    if (self) {
        cellSize = itemSize;
        arrData = array;
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        _collectionView=[[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
       // _collectionView.scrollEnabled = NO;
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        
        [_collectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        
        [_collectionView registerClass:[cellClass class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_collectionView];
    }
    return self;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    if ([cell isKindOfClass:[HomeCollectionCell class]]) {
        HomeCollectionCell *homeCell = (HomeCollectionCell *)cell;
        SceneListModel *model = arrData[indexPath.row];
        homeCell.lblTitle.text = model.sceneName;
    }
    else if ([cell isKindOfClass:[FavouriteAccessoriesCell class]]) {
        FavouriteAccessoriesCell *homeCell = (FavouriteAccessoriesCell *)cell;
        PointListModel *model = arrData[indexPath.row];
        homeCell.lblTitle.text = model.serviceName;
        homeCell.lblMsp.text = [NSString stringWithFormat:@"%@%@",@(model.msp),@"%"];
    }
    else if ([cell isKindOfClass:[CustomCollectionCell class]])
    {
        CustomCollectionCell *roomCell = (CustomCollectionCell *)cell;
        RoomListModel *model = arrData[indexPath.row];
        roomCell.lblTitle.text = model.roomName;
        roomCell.lblSubTitle.text = [NSString stringWithFormat:@"%ld Accessories",indexPath.row+1];
    }
    else if ([cell isKindOfClass:[AtomationCollectionCell class]])
    {
        AtomationCollectionCell *atomationCell = (AtomationCollectionCell *)cell;
        atomationCell.lblTitle.text = arrData[indexPath.row];
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.itemSelectBlock) {
        self.itemSelectBlock(indexPath.row);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return cellSize;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    CGRect selfFrame = self.frame;
    selfFrame.size.height = _collectionView.contentSize.height;
    self.frame = selfFrame;
    
    CGRect collectionFrame = _collectionView.frame;
    collectionFrame.size.height = self.frame.size.height+10;
    _collectionView.frame = collectionFrame;
    if (self.block) {
        self.block();
    }
}

-(void)onContentItemChange:(ContentChangeBlock)blk
{
    self.block = blk;
}

-(void)onItemSelect:(ItemSelectBlock)blk
{
    self.itemSelectBlock = blk;
}
@end

