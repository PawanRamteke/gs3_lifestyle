//
//  AppDelegate.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "DashboardViewController.h"
#import "SplashViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self setupNavigationBar];
    VIEWMANAGER.appManagedObjectContext = self.persistentContainer.viewContext;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"%@",[paths objectAtIndex:0]);
    
    NSDictionary *response = [Preferences getLoginResponse];
    if (response == nil) {
        [self setLoginAsRoot];
    }
    else{
        LoginModel *model = [LoginModel modelObjectWithDictionary:response];
        VIEWMANAGER.currentUser = model;
        [self setSpashAsRoot];
    }
    return YES;
}

-(void)setLoginAsRoot
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    UINavigationController *navigationVC = [[UINavigationController alloc]initWithRootViewController:loginVC];
    
    [UIView transitionWithView:self.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.window.rootViewController = navigationVC;
                    }
                    completion:nil];
    [self.window makeKeyAndVisible];
}

-(void)setSpashAsRoot
{
    SplashViewController *loginVC = [[SplashViewController alloc]init];
    UINavigationController *navigationVC = [[UINavigationController alloc]initWithRootViewController:loginVC];
    
    [UIView transitionWithView:self.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.window.rootViewController = navigationVC;
                    }
                    completion:nil];
    [self.window makeKeyAndVisible];
}


-(void)setDashboardAsRoot
{
    DashboardViewController *dashboardVC = [[DashboardViewController alloc]init];
    UINavigationController *navigationVC = [[UINavigationController alloc]initWithRootViewController:dashboardVC];
    
    [UIView transitionWithView:self.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.window.rootViewController = navigationVC;
                    }
                    completion:nil];
    [self.window makeKeyAndVisible];
}

- (void)setupNavigationBar
{
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    navigationBar.translucent = NO;
    navigationBar.barStyle = UIBarStyleDefault;
    navigationBar.tintColor = [UIColor appYellowColor];
    navigationBar.barTintColor = [UIColor appYellowColor];
    
    // title
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0]};
    navigationBar.titleTextAttributes = attributes;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -70) forBarMetrics:UIBarMetricsDefault];
    
    if(@available(iOS 11, *)) {
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateHighlighted];
        
    } else {
        [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60) forBarMetrics:UIBarMetricsDefault];
    }
    
    UIImage *back = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    navigationBar.backIndicatorImage = back;
    navigationBar.backIndicatorTransitionMaskImage = back;
}

-(void)callWebServices
{
    [self getSpaceListService];
}


-(void)getSpaceListService
{
    NSString *url = [NSString stringWithFormat:@"%@?owner=%@",GET_SPACE_URL,VIEWMANAGER.currentUser.userId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        
        NSArray *arr = (NSArray *)responseObject;
        SpaceListModel *spaceModel = arr[0];
        VIEWMANAGER.spaceId = spaceModel.spaceID;
        [self getDeviceList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [self getDeviceList];
         
    }];
}

-(void)getDeviceList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_DEVICE_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        [self getSceneList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getSceneList];
    }];
}

-(void)getSceneList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_SCENE_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        
        [self getFavouriteSceneList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getFavouriteSceneList];
    }];
}

-(void)getFavouriteSceneList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_FAVORITE_SCENE_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        VIEWMANAGER.arrFavouriteScene = responseObject;
        [self getPointList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getPointList];
    }];
}

-(void)getPointList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_POINT_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        
        [self getFavouritePointList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getFavouritePointList];
    }];
}

-(void)getFavouritePointList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_FAVORITE_POINT_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        
        [self getRoomList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getRoomList];
    }];
}

-(void)getRoomList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_ROOM_LIST_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        VIEWMANAGER.arrRoomList = responseObject;
        [self getSceneMappingList];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [self getSceneMappingList];
    }];
    
}

-(void)getSceneMappingList
{
    NSString *url = [NSString stringWithFormat:@"%@?spaceID=%d",GET_SCENE_MAPPING_URL,(int)VIEWMANAGER.spaceId];
    
    [REMOTE_API CallGETWebServiceWithParam:url params:nil sBlock:^(id responseObject) {
        [VIEWMANAGER hideAcytivityIndicator];
        [self setDashboardAsRoot];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [VIEWMANAGER hideAcytivityIndicator];
        [self setDashboardAsRoot];
    }];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"GS3_Lifestyle"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
