//
//  PointListModel.m
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "PointListModel.h"


NSString *const kPointListModelCurrent = @"current";
NSString *const kPointListModelGs3DevicePointscol = @"gs3_devicePointscol";
NSString *const kPointListModelPointType = @"pointType";
NSString *const kPointListModelServicePoint = @"servicePoint";
NSString *const kPointListModelWatt = @"watt";
NSString *const kPointListModelMsp = @"msp";
NSString *const kPointListModelApplianceSubType = @"applianceSubType";
NSString *const kPointListModelMode = @"mode";
NSString *const kPointListModelIcon = @"icon";
NSString *const kPointListModelOperations = @"operations";
NSString *const kPointListModelPointID = @"pointID";
NSString *const kPointListModelApplianceType = @"applianceType";
NSString *const kPointListModelMake = @"make";
NSString *const kPointListModelDeviceID = @"deviceID";
NSString *const kPointListModelServiceName = @"serviceName";
NSString *const kPointListModelModelNo = @"modelNo";


@interface PointListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PointListModel

@synthesize current = _current;
@synthesize gs3DevicePointscol = _gs3DevicePointscol;
@synthesize pointType = _pointType;
@synthesize servicePoint = _servicePoint;
@synthesize watt = _watt;
@synthesize msp = _msp;
@synthesize applianceSubType = _applianceSubType;
@synthesize mode = _mode;
@synthesize icon = _icon;
@synthesize operations = _operations;
@synthesize pointID = _pointID;
@synthesize applianceType = _applianceType;
@synthesize make = _make;
@synthesize deviceID = _deviceID;
@synthesize serviceName = _serviceName;
@synthesize modelNo = _modelNo;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.current = [self objectOrNilForKey:kPointListModelCurrent fromDictionary:dict];
            self.gs3DevicePointscol = [self objectOrNilForKey:kPointListModelGs3DevicePointscol fromDictionary:dict];
            self.pointType = [self objectOrNilForKey:kPointListModelPointType fromDictionary:dict];
            self.servicePoint = [[self objectOrNilForKey:kPointListModelServicePoint fromDictionary:dict] doubleValue];
            self.watt = [self objectOrNilForKey:kPointListModelWatt fromDictionary:dict];
            self.msp = [[self objectOrNilForKey:kPointListModelMsp fromDictionary:dict] doubleValue];
            self.applianceSubType = [self objectOrNilForKey:kPointListModelApplianceSubType fromDictionary:dict];
            self.mode = [[self objectOrNilForKey:kPointListModelMode fromDictionary:dict] doubleValue];
            self.icon = [self objectOrNilForKey:kPointListModelIcon fromDictionary:dict];
            self.operations = [[self objectOrNilForKey:kPointListModelOperations fromDictionary:dict] doubleValue];
            self.pointID = [[self objectOrNilForKey:kPointListModelPointID fromDictionary:dict] doubleValue];
            self.applianceType = [self objectOrNilForKey:kPointListModelApplianceType fromDictionary:dict];
            self.make = [self objectOrNilForKey:kPointListModelMake fromDictionary:dict];
            self.deviceID = [[self objectOrNilForKey:kPointListModelDeviceID fromDictionary:dict] doubleValue];
            self.serviceName = [self objectOrNilForKey:kPointListModelServiceName fromDictionary:dict];
            self.modelNo = [self objectOrNilForKey:kPointListModelModelNo fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.current forKey:kPointListModelCurrent];
    [mutableDict setValue:self.gs3DevicePointscol forKey:kPointListModelGs3DevicePointscol];
    [mutableDict setValue:self.pointType forKey:kPointListModelPointType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.servicePoint] forKey:kPointListModelServicePoint];
    [mutableDict setValue:self.watt forKey:kPointListModelWatt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.msp] forKey:kPointListModelMsp];
    [mutableDict setValue:self.applianceSubType forKey:kPointListModelApplianceSubType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mode] forKey:kPointListModelMode];
    [mutableDict setValue:self.icon forKey:kPointListModelIcon];
    [mutableDict setValue:[NSNumber numberWithDouble:self.operations] forKey:kPointListModelOperations];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pointID] forKey:kPointListModelPointID];
    [mutableDict setValue:self.applianceType forKey:kPointListModelApplianceType];
    [mutableDict setValue:self.make forKey:kPointListModelMake];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceID] forKey:kPointListModelDeviceID];
    [mutableDict setValue:self.serviceName forKey:kPointListModelServiceName];
    [mutableDict setValue:self.modelNo forKey:kPointListModelModelNo];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.current = [aDecoder decodeObjectForKey:kPointListModelCurrent];
    self.gs3DevicePointscol = [aDecoder decodeObjectForKey:kPointListModelGs3DevicePointscol];
    self.pointType = [aDecoder decodeObjectForKey:kPointListModelPointType];
    self.servicePoint = [aDecoder decodeDoubleForKey:kPointListModelServicePoint];
    self.watt = [aDecoder decodeObjectForKey:kPointListModelWatt];
    self.msp = [aDecoder decodeDoubleForKey:kPointListModelMsp];
    self.applianceSubType = [aDecoder decodeObjectForKey:kPointListModelApplianceSubType];
    self.mode = [aDecoder decodeDoubleForKey:kPointListModelMode];
    self.icon = [aDecoder decodeObjectForKey:kPointListModelIcon];
    self.operations = [aDecoder decodeDoubleForKey:kPointListModelOperations];
    self.pointID = [aDecoder decodeDoubleForKey:kPointListModelPointID];
    self.applianceType = [aDecoder decodeObjectForKey:kPointListModelApplianceType];
    self.make = [aDecoder decodeObjectForKey:kPointListModelMake];
    self.deviceID = [aDecoder decodeDoubleForKey:kPointListModelDeviceID];
    self.serviceName = [aDecoder decodeObjectForKey:kPointListModelServiceName];
    self.modelNo = [aDecoder decodeObjectForKey:kPointListModelModelNo];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_current forKey:kPointListModelCurrent];
    [aCoder encodeObject:_gs3DevicePointscol forKey:kPointListModelGs3DevicePointscol];
    [aCoder encodeObject:_pointType forKey:kPointListModelPointType];
    [aCoder encodeDouble:_servicePoint forKey:kPointListModelServicePoint];
    [aCoder encodeObject:_watt forKey:kPointListModelWatt];
    [aCoder encodeDouble:_msp forKey:kPointListModelMsp];
    [aCoder encodeObject:_applianceSubType forKey:kPointListModelApplianceSubType];
    [aCoder encodeDouble:_mode forKey:kPointListModelMode];
    [aCoder encodeObject:_icon forKey:kPointListModelIcon];
    [aCoder encodeDouble:_operations forKey:kPointListModelOperations];
    [aCoder encodeDouble:_pointID forKey:kPointListModelPointID];
    [aCoder encodeObject:_applianceType forKey:kPointListModelApplianceType];
    [aCoder encodeObject:_make forKey:kPointListModelMake];
    [aCoder encodeDouble:_deviceID forKey:kPointListModelDeviceID];
    [aCoder encodeObject:_serviceName forKey:kPointListModelServiceName];
    [aCoder encodeObject:_modelNo forKey:kPointListModelModelNo];
}

- (id)copyWithZone:(NSZone *)zone
{
    PointListModel *copy = [[PointListModel alloc] init];
    
    if (copy) {

        copy.current = [self.current copyWithZone:zone];
        copy.gs3DevicePointscol = [self.gs3DevicePointscol copyWithZone:zone];
        copy.pointType = [self.pointType copyWithZone:zone];
        copy.servicePoint = self.servicePoint;
        copy.watt = [self.watt copyWithZone:zone];
        copy.msp = self.msp;
        copy.applianceSubType = [self.applianceSubType copyWithZone:zone];
        copy.mode = self.mode;
        copy.icon = [self.icon copyWithZone:zone];
        copy.operations = self.operations;
        copy.pointID = self.pointID;
        copy.applianceType = [self.applianceType copyWithZone:zone];
        copy.make = [self.make copyWithZone:zone];
        copy.deviceID = self.deviceID;
        copy.serviceName = [self.serviceName copyWithZone:zone];
        copy.modelNo = [self.modelNo copyWithZone:zone];
    }
    
    return copy;
}


@end
