//
//  SpaceListModel.m
//
//  Created by Pawan Ramteke on 01/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SpaceListModel.h"


NSString *const kSpaceListModelOwner = @"owner";
NSString *const kSpaceListModelCity = @"city";
NSString *const kSpaceListModelLongitude = @"longitude";
NSString *const kSpaceListModelSpaceID = @"spaceID";
NSString *const kSpaceListModelIsActiveSpace = @"isActiveSpace";
NSString *const kSpaceListModelAddress = @"Address";
NSString *const kSpaceListModelLatitude = @"latitude";
NSString *const kSpaceListModelImage = @"image";
NSString *const kSpaceListModelSpaceName = @"spaceName";
NSString *const kSpaceListModelIcon = @"icon";


@interface SpaceListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SpaceListModel

@synthesize owner = _owner;
@synthesize city = _city;
@synthesize longitude = _longitude;
@synthesize spaceID = _spaceID;
@synthesize isActiveSpace = _isActiveSpace;
@synthesize address = _address;
@synthesize latitude = _latitude;
@synthesize image = _image;
@synthesize spaceName = _spaceName;
@synthesize icon = _icon;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.owner = [[self objectOrNilForKey:kSpaceListModelOwner fromDictionary:dict] doubleValue];
            self.city = [self objectOrNilForKey:kSpaceListModelCity fromDictionary:dict];
            self.longitude = [self objectOrNilForKey:kSpaceListModelLongitude fromDictionary:dict];
            self.spaceID = [[self objectOrNilForKey:kSpaceListModelSpaceID fromDictionary:dict] doubleValue];
            self.isActiveSpace = [[self objectOrNilForKey:kSpaceListModelIsActiveSpace fromDictionary:dict] boolValue];
            self.address = [self objectOrNilForKey:kSpaceListModelAddress fromDictionary:dict];
            self.latitude = [self objectOrNilForKey:kSpaceListModelLatitude fromDictionary:dict];
            self.image = [self objectOrNilForKey:kSpaceListModelImage fromDictionary:dict];
            self.spaceName = [self objectOrNilForKey:kSpaceListModelSpaceName fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kSpaceListModelIcon fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.owner] forKey:kSpaceListModelOwner];
    [mutableDict setValue:self.city forKey:kSpaceListModelCity];
    [mutableDict setValue:self.longitude forKey:kSpaceListModelLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.spaceID] forKey:kSpaceListModelSpaceID];
    [mutableDict setValue:[NSNumber numberWithBool:self.isActiveSpace] forKey:kSpaceListModelIsActiveSpace];
    [mutableDict setValue:self.address forKey:kSpaceListModelAddress];
    [mutableDict setValue:self.latitude forKey:kSpaceListModelLatitude];
    [mutableDict setValue:self.image forKey:kSpaceListModelImage];
    [mutableDict setValue:self.spaceName forKey:kSpaceListModelSpaceName];
    [mutableDict setValue:self.icon forKey:kSpaceListModelIcon];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.owner = [aDecoder decodeDoubleForKey:kSpaceListModelOwner];
    self.city = [aDecoder decodeObjectForKey:kSpaceListModelCity];
    self.longitude = [aDecoder decodeObjectForKey:kSpaceListModelLongitude];
    self.spaceID = [aDecoder decodeDoubleForKey:kSpaceListModelSpaceID];
    self.isActiveSpace = [aDecoder decodeBoolForKey:kSpaceListModelIsActiveSpace];
    self.address = [aDecoder decodeObjectForKey:kSpaceListModelAddress];
    self.latitude = [aDecoder decodeObjectForKey:kSpaceListModelLatitude];
    self.image = [aDecoder decodeObjectForKey:kSpaceListModelImage];
    self.spaceName = [aDecoder decodeObjectForKey:kSpaceListModelSpaceName];
    self.icon = [aDecoder decodeObjectForKey:kSpaceListModelIcon];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_owner forKey:kSpaceListModelOwner];
    [aCoder encodeObject:_city forKey:kSpaceListModelCity];
    [aCoder encodeObject:_longitude forKey:kSpaceListModelLongitude];
    [aCoder encodeDouble:_spaceID forKey:kSpaceListModelSpaceID];
    [aCoder encodeBool:_isActiveSpace forKey:kSpaceListModelIsActiveSpace];
    [aCoder encodeObject:_address forKey:kSpaceListModelAddress];
    [aCoder encodeObject:_latitude forKey:kSpaceListModelLatitude];
    [aCoder encodeObject:_image forKey:kSpaceListModelImage];
    [aCoder encodeObject:_spaceName forKey:kSpaceListModelSpaceName];
    [aCoder encodeObject:_icon forKey:kSpaceListModelIcon];
}

- (id)copyWithZone:(NSZone *)zone
{
    SpaceListModel *copy = [[SpaceListModel alloc] init];
    
    if (copy) {

        copy.owner = self.owner;
        copy.city = [self.city copyWithZone:zone];
        copy.longitude = [self.longitude copyWithZone:zone];
        copy.spaceID = self.spaceID;
        copy.isActiveSpace = self.isActiveSpace;
        copy.address = [self.address copyWithZone:zone];
        copy.latitude = [self.latitude copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.spaceName = [self.spaceName copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
    }
    
    return copy;
}


@end
