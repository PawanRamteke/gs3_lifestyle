//
//  DeviceListModel.m
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "DeviceListModel.h"


NSString *const kDeviceListModelPrevEDIPAddress = @"prevED_IPAddress";
NSString *const kDeviceListModelSpaceID = @"spaceID";
NSString *const kDeviceListModelDeviceName = @"deviceName";
NSString *const kDeviceListModelOwner = @"owner";
NSString *const kDeviceListModelGUID = @"GUID";
NSString *const kDeviceListModelApPassword = @"ap_password";
NSString *const kDeviceListModelDeviceSerialNo = @"deviceSerialNo";
NSString *const kDeviceListModelLatitude = @"latitude";
NSString *const kDeviceListModelImei = @"imei";
NSString *const kDeviceListModelDeviceStatus = @"deviceStatus";
NSString *const kDeviceListModelActivationDate = @"activationDate";
NSString *const kDeviceListModelPrevAPIPAddress = @"prevAP_IPAddress";
NSString *const kDeviceListModelCipher = @"cipher";
NSString *const kDeviceListModelDeviceID = @"deviceID";
NSString *const kDeviceListModelLastRechDate = @"lastRechDate";
NSString *const kDeviceListModelEdIPAddress = @"ed_IPAddress";
NSString *const kDeviceListModelApIPAddress = @"ap_IPAddress";
NSString *const kDeviceListModelLongitude = @"longitude";
NSString *const kDeviceListModelRoomID = @"roomID";
NSString *const kDeviceListModelMobile = @"mobile";
NSString *const kDeviceListModelDeviceModel = @"deviceModel";
NSString *const kDeviceListModelEdPassword = @"ed_Password";
NSString *const kDeviceListModelMacAddress = @"macAddress";
NSString *const kDeviceListModelApSSID = @"ap_SSID";
NSString *const kDeviceListModelEdSSID = @"ed_SSID";
NSString *const kDeviceListModelZoneID = @"zoneID";


@interface DeviceListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation DeviceListModel

@synthesize prevEDIPAddress = _prevEDIPAddress;
@synthesize spaceID = _spaceID;
@synthesize deviceName = _deviceName;
@synthesize owner = _owner;
@synthesize gUID = _gUID;
@synthesize apPassword = _apPassword;
@synthesize deviceSerialNo = _deviceSerialNo;
@synthesize latitude = _latitude;
@synthesize imei = _imei;
@synthesize deviceStatus = _deviceStatus;
@synthesize activationDate = _activationDate;
@synthesize prevAPIPAddress = _prevAPIPAddress;
@synthesize cipher = _cipher;
@synthesize deviceID = _deviceID;
@synthesize lastRechDate = _lastRechDate;
@synthesize edIPAddress = _edIPAddress;
@synthesize apIPAddress = _apIPAddress;
@synthesize longitude = _longitude;
@synthesize roomID = _roomID;
@synthesize mobile = _mobile;
@synthesize deviceModel = _deviceModel;
@synthesize edPassword = _edPassword;
@synthesize macAddress = _macAddress;
@synthesize apSSID = _apSSID;
@synthesize edSSID = _edSSID;
@synthesize zoneID = _zoneID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.prevEDIPAddress = [self objectOrNilForKey:kDeviceListModelPrevEDIPAddress fromDictionary:dict];
            self.spaceID = [[self objectOrNilForKey:kDeviceListModelSpaceID fromDictionary:dict] doubleValue];
            self.deviceName = [self objectOrNilForKey:kDeviceListModelDeviceName fromDictionary:dict];
            self.owner = [[self objectOrNilForKey:kDeviceListModelOwner fromDictionary:dict] doubleValue];
            self.gUID = [self objectOrNilForKey:kDeviceListModelGUID fromDictionary:dict];
            self.apPassword = [self objectOrNilForKey:kDeviceListModelApPassword fromDictionary:dict];
            self.deviceSerialNo = [self objectOrNilForKey:kDeviceListModelDeviceSerialNo fromDictionary:dict];
            self.latitude = [self objectOrNilForKey:kDeviceListModelLatitude fromDictionary:dict];
            self.imei = [self objectOrNilForKey:kDeviceListModelImei fromDictionary:dict];
            self.deviceStatus = [[self objectOrNilForKey:kDeviceListModelDeviceStatus fromDictionary:dict] boolValue];
            self.activationDate = [self objectOrNilForKey:kDeviceListModelActivationDate fromDictionary:dict];
            self.prevAPIPAddress = [self objectOrNilForKey:kDeviceListModelPrevAPIPAddress fromDictionary:dict];
            self.cipher = [self objectOrNilForKey:kDeviceListModelCipher fromDictionary:dict];
            self.deviceID = [[self objectOrNilForKey:kDeviceListModelDeviceID fromDictionary:dict] doubleValue];
            self.lastRechDate = [self objectOrNilForKey:kDeviceListModelLastRechDate fromDictionary:dict];
            self.edIPAddress = [self objectOrNilForKey:kDeviceListModelEdIPAddress fromDictionary:dict];
            self.apIPAddress = [self objectOrNilForKey:kDeviceListModelApIPAddress fromDictionary:dict];
            self.longitude = [self objectOrNilForKey:kDeviceListModelLongitude fromDictionary:dict];
            self.roomID = [[self objectOrNilForKey:kDeviceListModelRoomID fromDictionary:dict] doubleValue];
            self.mobile = [self objectOrNilForKey:kDeviceListModelMobile fromDictionary:dict];
            self.deviceModel = [self objectOrNilForKey:kDeviceListModelDeviceModel fromDictionary:dict];
            self.edPassword = [self objectOrNilForKey:kDeviceListModelEdPassword fromDictionary:dict];
            self.macAddress = [self objectOrNilForKey:kDeviceListModelMacAddress fromDictionary:dict];
            self.apSSID = [self objectOrNilForKey:kDeviceListModelApSSID fromDictionary:dict];
            self.edSSID = [self objectOrNilForKey:kDeviceListModelEdSSID fromDictionary:dict];
            self.zoneID = [[self objectOrNilForKey:kDeviceListModelZoneID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.prevEDIPAddress forKey:kDeviceListModelPrevEDIPAddress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.spaceID] forKey:kDeviceListModelSpaceID];
    [mutableDict setValue:self.deviceName forKey:kDeviceListModelDeviceName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.owner] forKey:kDeviceListModelOwner];
    [mutableDict setValue:self.gUID forKey:kDeviceListModelGUID];
    [mutableDict setValue:self.apPassword forKey:kDeviceListModelApPassword];
    [mutableDict setValue:self.deviceSerialNo forKey:kDeviceListModelDeviceSerialNo];
    [mutableDict setValue:self.latitude forKey:kDeviceListModelLatitude];
    [mutableDict setValue:self.imei forKey:kDeviceListModelImei];
    [mutableDict setValue:[NSNumber numberWithBool:self.deviceStatus] forKey:kDeviceListModelDeviceStatus];
    [mutableDict setValue:self.activationDate forKey:kDeviceListModelActivationDate];
    [mutableDict setValue:self.prevAPIPAddress forKey:kDeviceListModelPrevAPIPAddress];
    [mutableDict setValue:self.cipher forKey:kDeviceListModelCipher];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceID] forKey:kDeviceListModelDeviceID];
    [mutableDict setValue:self.lastRechDate forKey:kDeviceListModelLastRechDate];
    [mutableDict setValue:self.edIPAddress forKey:kDeviceListModelEdIPAddress];
    [mutableDict setValue:self.apIPAddress forKey:kDeviceListModelApIPAddress];
    [mutableDict setValue:self.longitude forKey:kDeviceListModelLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.roomID] forKey:kDeviceListModelRoomID];
    [mutableDict setValue:self.mobile forKey:kDeviceListModelMobile];
    [mutableDict setValue:self.deviceModel forKey:kDeviceListModelDeviceModel];
    [mutableDict setValue:self.edPassword forKey:kDeviceListModelEdPassword];
    [mutableDict setValue:self.macAddress forKey:kDeviceListModelMacAddress];
    [mutableDict setValue:self.apSSID forKey:kDeviceListModelApSSID];
    [mutableDict setValue:self.edSSID forKey:kDeviceListModelEdSSID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.zoneID] forKey:kDeviceListModelZoneID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.prevEDIPAddress = [aDecoder decodeObjectForKey:kDeviceListModelPrevEDIPAddress];
    self.spaceID = [aDecoder decodeDoubleForKey:kDeviceListModelSpaceID];
    self.deviceName = [aDecoder decodeObjectForKey:kDeviceListModelDeviceName];
    self.owner = [aDecoder decodeDoubleForKey:kDeviceListModelOwner];
    self.gUID = [aDecoder decodeObjectForKey:kDeviceListModelGUID];
    self.apPassword = [aDecoder decodeObjectForKey:kDeviceListModelApPassword];
    self.deviceSerialNo = [aDecoder decodeObjectForKey:kDeviceListModelDeviceSerialNo];
    self.latitude = [aDecoder decodeObjectForKey:kDeviceListModelLatitude];
    self.imei = [aDecoder decodeObjectForKey:kDeviceListModelImei];
    self.deviceStatus = [aDecoder decodeBoolForKey:kDeviceListModelDeviceStatus];
    self.activationDate = [aDecoder decodeObjectForKey:kDeviceListModelActivationDate];
    self.prevAPIPAddress = [aDecoder decodeObjectForKey:kDeviceListModelPrevAPIPAddress];
    self.cipher = [aDecoder decodeObjectForKey:kDeviceListModelCipher];
    self.deviceID = [aDecoder decodeDoubleForKey:kDeviceListModelDeviceID];
    self.lastRechDate = [aDecoder decodeObjectForKey:kDeviceListModelLastRechDate];
    self.edIPAddress = [aDecoder decodeObjectForKey:kDeviceListModelEdIPAddress];
    self.apIPAddress = [aDecoder decodeObjectForKey:kDeviceListModelApIPAddress];
    self.longitude = [aDecoder decodeObjectForKey:kDeviceListModelLongitude];
    self.roomID = [aDecoder decodeDoubleForKey:kDeviceListModelRoomID];
    self.mobile = [aDecoder decodeObjectForKey:kDeviceListModelMobile];
    self.deviceModel = [aDecoder decodeObjectForKey:kDeviceListModelDeviceModel];
    self.edPassword = [aDecoder decodeObjectForKey:kDeviceListModelEdPassword];
    self.macAddress = [aDecoder decodeObjectForKey:kDeviceListModelMacAddress];
    self.apSSID = [aDecoder decodeObjectForKey:kDeviceListModelApSSID];
    self.edSSID = [aDecoder decodeObjectForKey:kDeviceListModelEdSSID];
    self.zoneID = [aDecoder decodeDoubleForKey:kDeviceListModelZoneID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_prevEDIPAddress forKey:kDeviceListModelPrevEDIPAddress];
    [aCoder encodeDouble:_spaceID forKey:kDeviceListModelSpaceID];
    [aCoder encodeObject:_deviceName forKey:kDeviceListModelDeviceName];
    [aCoder encodeDouble:_owner forKey:kDeviceListModelOwner];
    [aCoder encodeObject:_gUID forKey:kDeviceListModelGUID];
    [aCoder encodeObject:_apPassword forKey:kDeviceListModelApPassword];
    [aCoder encodeObject:_deviceSerialNo forKey:kDeviceListModelDeviceSerialNo];
    [aCoder encodeObject:_latitude forKey:kDeviceListModelLatitude];
    [aCoder encodeObject:_imei forKey:kDeviceListModelImei];
    [aCoder encodeBool:_deviceStatus forKey:kDeviceListModelDeviceStatus];
    [aCoder encodeObject:_activationDate forKey:kDeviceListModelActivationDate];
    [aCoder encodeObject:_prevAPIPAddress forKey:kDeviceListModelPrevAPIPAddress];
    [aCoder encodeObject:_cipher forKey:kDeviceListModelCipher];
    [aCoder encodeDouble:_deviceID forKey:kDeviceListModelDeviceID];
    [aCoder encodeObject:_lastRechDate forKey:kDeviceListModelLastRechDate];
    [aCoder encodeObject:_edIPAddress forKey:kDeviceListModelEdIPAddress];
    [aCoder encodeObject:_apIPAddress forKey:kDeviceListModelApIPAddress];
    [aCoder encodeObject:_longitude forKey:kDeviceListModelLongitude];
    [aCoder encodeDouble:_roomID forKey:kDeviceListModelRoomID];
    [aCoder encodeObject:_mobile forKey:kDeviceListModelMobile];
    [aCoder encodeObject:_deviceModel forKey:kDeviceListModelDeviceModel];
    [aCoder encodeObject:_edPassword forKey:kDeviceListModelEdPassword];
    [aCoder encodeObject:_macAddress forKey:kDeviceListModelMacAddress];
    [aCoder encodeObject:_apSSID forKey:kDeviceListModelApSSID];
    [aCoder encodeObject:_edSSID forKey:kDeviceListModelEdSSID];
    [aCoder encodeDouble:_zoneID forKey:kDeviceListModelZoneID];
}

- (id)copyWithZone:(NSZone *)zone
{
    DeviceListModel *copy = [[DeviceListModel alloc] init];
    
    if (copy) {

        copy.prevEDIPAddress = [self.prevEDIPAddress copyWithZone:zone];
        copy.spaceID = self.spaceID;
        copy.deviceName = [self.deviceName copyWithZone:zone];
        copy.owner = self.owner;
        copy.gUID = [self.gUID copyWithZone:zone];
        copy.apPassword = [self.apPassword copyWithZone:zone];
        copy.deviceSerialNo = [self.deviceSerialNo copyWithZone:zone];
        copy.latitude = [self.latitude copyWithZone:zone];
        copy.imei = [self.imei copyWithZone:zone];
        copy.deviceStatus = self.deviceStatus;
        copy.activationDate = [self.activationDate copyWithZone:zone];
        copy.prevAPIPAddress = [self.prevAPIPAddress copyWithZone:zone];
        copy.cipher = [self.cipher copyWithZone:zone];
        copy.deviceID = self.deviceID;
        copy.lastRechDate = [self.lastRechDate copyWithZone:zone];
        copy.edIPAddress = [self.edIPAddress copyWithZone:zone];
        copy.apIPAddress = [self.apIPAddress copyWithZone:zone];
        copy.longitude = [self.longitude copyWithZone:zone];
        copy.roomID = self.roomID;
        copy.mobile = [self.mobile copyWithZone:zone];
        copy.deviceModel = [self.deviceModel copyWithZone:zone];
        copy.edPassword = [self.edPassword copyWithZone:zone];
        copy.macAddress = [self.macAddress copyWithZone:zone];
        copy.apSSID = [self.apSSID copyWithZone:zone];
        copy.edSSID = [self.edSSID copyWithZone:zone];
        copy.zoneID = self.zoneID;
    }
    
    return copy;
}


@end
