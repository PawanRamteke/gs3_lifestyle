//
//  LoginModel.m
//
//  Created by Pawan Ramteke on 01/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "LoginModel.h"


NSString *const kLoginModelUserId = @"UserId";
NSString *const kLoginModelSpacename = @"spacename";
NSString *const kLoginModelRole = @"Role";
NSString *const kLoginModelTokenType = @"token_type";
NSString *const kLoginModelIssued = @".issued";
NSString *const kLoginModelSpace = @"space";
NSString *const kLoginModelExpires = @".expires";
NSString *const kLoginModelUserName = @"UserName";
NSString *const kLoginModelAccessToken = @"access_token";
NSString *const kLoginModelExpiresIn = @"expires_in";


@interface LoginModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LoginModel

@synthesize userId = _userId;
@synthesize spacename = _spacename;
@synthesize role = _role;
@synthesize tokenType = _tokenType;
@synthesize issued = _issued;
@synthesize space = _space;
@synthesize expires = _expires;
@synthesize userName = _userName;
@synthesize accessToken = _accessToken;
@synthesize expiresIn = _expiresIn;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userId = [self objectOrNilForKey:kLoginModelUserId fromDictionary:dict];
            self.spacename = [self objectOrNilForKey:kLoginModelSpacename fromDictionary:dict];
            self.role = [self objectOrNilForKey:kLoginModelRole fromDictionary:dict];
            self.tokenType = [self objectOrNilForKey:kLoginModelTokenType fromDictionary:dict];
            self.issued = [self objectOrNilForKey:kLoginModelIssued fromDictionary:dict];
            self.space = [self objectOrNilForKey:kLoginModelSpace fromDictionary:dict];
            self.expires = [self objectOrNilForKey:kLoginModelExpires fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kLoginModelUserName fromDictionary:dict];
            self.accessToken = [self objectOrNilForKey:kLoginModelAccessToken fromDictionary:dict];
            self.expiresIn = [[self objectOrNilForKey:kLoginModelExpiresIn fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userId forKey:kLoginModelUserId];
    [mutableDict setValue:self.spacename forKey:kLoginModelSpacename];
    [mutableDict setValue:self.role forKey:kLoginModelRole];
    [mutableDict setValue:self.tokenType forKey:kLoginModelTokenType];
    [mutableDict setValue:self.issued forKey:kLoginModelIssued];
    [mutableDict setValue:self.space forKey:kLoginModelSpace];
    [mutableDict setValue:self.expires forKey:kLoginModelExpires];
    [mutableDict setValue:self.userName forKey:kLoginModelUserName];
    [mutableDict setValue:self.accessToken forKey:kLoginModelAccessToken];
    [mutableDict setValue:[NSNumber numberWithDouble:self.expiresIn] forKey:kLoginModelExpiresIn];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userId = [aDecoder decodeObjectForKey:kLoginModelUserId];
    self.spacename = [aDecoder decodeObjectForKey:kLoginModelSpacename];
    self.role = [aDecoder decodeObjectForKey:kLoginModelRole];
    self.tokenType = [aDecoder decodeObjectForKey:kLoginModelTokenType];
    self.issued = [aDecoder decodeObjectForKey:kLoginModelIssued];
    self.space = [aDecoder decodeObjectForKey:kLoginModelSpace];
    self.expires = [aDecoder decodeObjectForKey:kLoginModelExpires];
    self.userName = [aDecoder decodeObjectForKey:kLoginModelUserName];
    self.accessToken = [aDecoder decodeObjectForKey:kLoginModelAccessToken];
    self.expiresIn = [aDecoder decodeDoubleForKey:kLoginModelExpiresIn];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userId forKey:kLoginModelUserId];
    [aCoder encodeObject:_spacename forKey:kLoginModelSpacename];
    [aCoder encodeObject:_role forKey:kLoginModelRole];
    [aCoder encodeObject:_tokenType forKey:kLoginModelTokenType];
    [aCoder encodeObject:_issued forKey:kLoginModelIssued];
    [aCoder encodeObject:_space forKey:kLoginModelSpace];
    [aCoder encodeObject:_expires forKey:kLoginModelExpires];
    [aCoder encodeObject:_userName forKey:kLoginModelUserName];
    [aCoder encodeObject:_accessToken forKey:kLoginModelAccessToken];
    [aCoder encodeDouble:_expiresIn forKey:kLoginModelExpiresIn];
}

- (id)copyWithZone:(NSZone *)zone
{
    LoginModel *copy = [[LoginModel alloc] init];
    
    if (copy) {

        copy.userId = [self.userId copyWithZone:zone];
        copy.spacename = [self.spacename copyWithZone:zone];
        copy.role = [self.role copyWithZone:zone];
        copy.tokenType = [self.tokenType copyWithZone:zone];
        copy.issued = [self.issued copyWithZone:zone];
        copy.space = [self.space copyWithZone:zone];
        copy.expires = [self.expires copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
        copy.accessToken = [self.accessToken copyWithZone:zone];
        copy.expiresIn = self.expiresIn;
    }
    
    return copy;
}


@end
