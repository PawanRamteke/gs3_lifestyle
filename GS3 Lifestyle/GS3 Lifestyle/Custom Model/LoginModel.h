//
//  LoginModel.h
//
//  Created by Pawan Ramteke on 01/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LoginModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *spacename;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *tokenType;
@property (nonatomic, strong) NSString *issued;
@property (nonatomic, strong) NSString *space;
@property (nonatomic, strong) NSString *expires;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, assign) double expiresIn;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
