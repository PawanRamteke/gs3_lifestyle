//
//  PointListModel.h
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PointListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *current;
@property (nonatomic, strong) NSString *gs3DevicePointscol;
@property (nonatomic, strong) NSString *pointType;
@property (nonatomic, assign) double servicePoint;
@property (nonatomic, strong) NSString *watt;
@property (nonatomic, assign) double msp;
@property (nonatomic, strong) NSString *applianceSubType;
@property (nonatomic, assign) double mode;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) double operations;
@property (nonatomic, assign) double pointID;
@property (nonatomic, strong) NSString *applianceType;
@property (nonatomic, strong) NSString *make;
@property (nonatomic, assign) double deviceID;
@property (nonatomic, strong) NSString *serviceName;
@property (nonatomic, strong) NSString *modelNo;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
