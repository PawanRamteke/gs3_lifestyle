//
//  RoomListModel.m
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "RoomListModel.h"


NSString *const kRoomListModelZoneID = @"zoneID";
NSString *const kRoomListModelRoomID = @"roomID";
NSString *const kRoomListModelRoomName = @"roomName";
NSString *const kRoomListModelIcon = @"icon";
NSString *const kRoomListModelImage = @"image";
NSString *const kRoomListModelSpaceID = @"spaceID";


@interface RoomListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RoomListModel

@synthesize zoneID = _zoneID;
@synthesize roomID = _roomID;
@synthesize roomName = _roomName;
@synthesize icon = _icon;
@synthesize image = _image;
@synthesize spaceID = _spaceID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.zoneID = [[self objectOrNilForKey:kRoomListModelZoneID fromDictionary:dict] doubleValue];
            self.roomID = [[self objectOrNilForKey:kRoomListModelRoomID fromDictionary:dict] doubleValue];
            self.roomName = [self objectOrNilForKey:kRoomListModelRoomName fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kRoomListModelIcon fromDictionary:dict];
            self.image = [self objectOrNilForKey:kRoomListModelImage fromDictionary:dict];
            self.spaceID = [[self objectOrNilForKey:kRoomListModelSpaceID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.zoneID] forKey:kRoomListModelZoneID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.roomID] forKey:kRoomListModelRoomID];
    [mutableDict setValue:self.roomName forKey:kRoomListModelRoomName];
    [mutableDict setValue:self.icon forKey:kRoomListModelIcon];
    [mutableDict setValue:self.image forKey:kRoomListModelImage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.spaceID] forKey:kRoomListModelSpaceID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.zoneID = [aDecoder decodeDoubleForKey:kRoomListModelZoneID];
    self.roomID = [aDecoder decodeDoubleForKey:kRoomListModelRoomID];
    self.roomName = [aDecoder decodeObjectForKey:kRoomListModelRoomName];
    self.icon = [aDecoder decodeObjectForKey:kRoomListModelIcon];
    self.image = [aDecoder decodeObjectForKey:kRoomListModelImage];
    self.spaceID = [aDecoder decodeDoubleForKey:kRoomListModelSpaceID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_zoneID forKey:kRoomListModelZoneID];
    [aCoder encodeDouble:_roomID forKey:kRoomListModelRoomID];
    [aCoder encodeObject:_roomName forKey:kRoomListModelRoomName];
    [aCoder encodeObject:_icon forKey:kRoomListModelIcon];
    [aCoder encodeObject:_image forKey:kRoomListModelImage];
    [aCoder encodeDouble:_spaceID forKey:kRoomListModelSpaceID];
}

- (id)copyWithZone:(NSZone *)zone
{
    RoomListModel *copy = [[RoomListModel alloc] init];
    
    if (copy) {

        copy.zoneID = self.zoneID;
        copy.roomID = self.roomID;
        copy.roomName = [self.roomName copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.spaceID = self.spaceID;
    }
    
    return copy;
}


@end
