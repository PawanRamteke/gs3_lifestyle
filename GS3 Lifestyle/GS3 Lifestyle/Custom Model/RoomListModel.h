//
//  RoomListModel.h
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RoomListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double zoneID;
@property (nonatomic, assign) double roomID;
@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) id image;
@property (nonatomic, assign) double spaceID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
