//
//  SceneListModel.m
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SceneListModel.h"


NSString *const kSceneListModelSceneName = @"sceneName";
NSString *const kSceneListModelRoomid = @"roomid";
NSString *const kSceneListModelOwner = @"owner";
NSString *const kSceneListModelSceneID = @"sceneID";
NSString *const kSceneListModelSceneType = @"sceneType";
NSString *const kSceneListModelSpaceID = @"spaceID";
NSString *const kSceneListModelSwitchinput = @"switchinput";
NSString *const kSceneListModelIcon = @"icon";
NSString *const kSceneListModelIsEnabled = @"isEnabled";
NSString *const kSceneListModelSceneservice = @"sceneservice";
NSString *const kSceneListModelDeviceID = @"deviceID";
NSString *const kSceneListModelZoneid = @"zoneid";


@interface SceneListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SceneListModel

@synthesize sceneName = _sceneName;
@synthesize roomid = _roomid;
@synthesize owner = _owner;
@synthesize sceneID = _sceneID;
@synthesize sceneType = _sceneType;
@synthesize spaceID = _spaceID;
@synthesize switchinput = _switchinput;
@synthesize icon = _icon;
@synthesize isEnabled = _isEnabled;
@synthesize sceneservice = _sceneservice;
@synthesize deviceID = _deviceID;
@synthesize zoneid = _zoneid;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.sceneName = [self objectOrNilForKey:kSceneListModelSceneName fromDictionary:dict];
            self.roomid = [[self objectOrNilForKey:kSceneListModelRoomid fromDictionary:dict] doubleValue];
            self.owner = [[self objectOrNilForKey:kSceneListModelOwner fromDictionary:dict] doubleValue];
            self.sceneID = [[self objectOrNilForKey:kSceneListModelSceneID fromDictionary:dict] doubleValue];
            self.sceneType = [self objectOrNilForKey:kSceneListModelSceneType fromDictionary:dict];
            self.spaceID = [[self objectOrNilForKey:kSceneListModelSpaceID fromDictionary:dict] doubleValue];
            self.switchinput = [self objectOrNilForKey:kSceneListModelSwitchinput fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kSceneListModelIcon fromDictionary:dict];
            self.isEnabled = [[self objectOrNilForKey:kSceneListModelIsEnabled fromDictionary:dict] boolValue];
            self.sceneservice = [self objectOrNilForKey:kSceneListModelSceneservice fromDictionary:dict];
            self.deviceID = [[self objectOrNilForKey:kSceneListModelDeviceID fromDictionary:dict] doubleValue];
            self.zoneid = [[self objectOrNilForKey:kSceneListModelZoneid fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.sceneName forKey:kSceneListModelSceneName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.roomid] forKey:kSceneListModelRoomid];
    [mutableDict setValue:[NSNumber numberWithDouble:self.owner] forKey:kSceneListModelOwner];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sceneID] forKey:kSceneListModelSceneID];
    [mutableDict setValue:self.sceneType forKey:kSceneListModelSceneType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.spaceID] forKey:kSceneListModelSpaceID];
    [mutableDict setValue:self.switchinput forKey:kSceneListModelSwitchinput];
    [mutableDict setValue:self.icon forKey:kSceneListModelIcon];
    [mutableDict setValue:[NSNumber numberWithBool:self.isEnabled] forKey:kSceneListModelIsEnabled];
    [mutableDict setValue:self.sceneservice forKey:kSceneListModelSceneservice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceID] forKey:kSceneListModelDeviceID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.zoneid] forKey:kSceneListModelZoneid];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.sceneName = [aDecoder decodeObjectForKey:kSceneListModelSceneName];
    self.roomid = [aDecoder decodeDoubleForKey:kSceneListModelRoomid];
    self.owner = [aDecoder decodeDoubleForKey:kSceneListModelOwner];
    self.sceneID = [aDecoder decodeDoubleForKey:kSceneListModelSceneID];
    self.sceneType = [aDecoder decodeObjectForKey:kSceneListModelSceneType];
    self.spaceID = [aDecoder decodeDoubleForKey:kSceneListModelSpaceID];
    self.switchinput = [aDecoder decodeObjectForKey:kSceneListModelSwitchinput];
    self.icon = [aDecoder decodeObjectForKey:kSceneListModelIcon];
    self.isEnabled = [aDecoder decodeBoolForKey:kSceneListModelIsEnabled];
    self.sceneservice = [aDecoder decodeObjectForKey:kSceneListModelSceneservice];
    self.deviceID = [aDecoder decodeDoubleForKey:kSceneListModelDeviceID];
    self.zoneid = [aDecoder decodeDoubleForKey:kSceneListModelZoneid];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_sceneName forKey:kSceneListModelSceneName];
    [aCoder encodeDouble:_roomid forKey:kSceneListModelRoomid];
    [aCoder encodeDouble:_owner forKey:kSceneListModelOwner];
    [aCoder encodeDouble:_sceneID forKey:kSceneListModelSceneID];
    [aCoder encodeObject:_sceneType forKey:kSceneListModelSceneType];
    [aCoder encodeDouble:_spaceID forKey:kSceneListModelSpaceID];
    [aCoder encodeObject:_switchinput forKey:kSceneListModelSwitchinput];
    [aCoder encodeObject:_icon forKey:kSceneListModelIcon];
    [aCoder encodeBool:_isEnabled forKey:kSceneListModelIsEnabled];
    [aCoder encodeObject:_sceneservice forKey:kSceneListModelSceneservice];
    [aCoder encodeDouble:_deviceID forKey:kSceneListModelDeviceID];
    [aCoder encodeDouble:_zoneid forKey:kSceneListModelZoneid];
}

- (id)copyWithZone:(NSZone *)zone
{
    SceneListModel *copy = [[SceneListModel alloc] init];
    
    if (copy) {

        copy.sceneName = [self.sceneName copyWithZone:zone];
        copy.roomid = self.roomid;
        copy.owner = self.owner;
        copy.sceneID = self.sceneID;
        copy.sceneType = [self.sceneType copyWithZone:zone];
        copy.spaceID = self.spaceID;
        copy.switchinput = [self.switchinput copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.isEnabled = self.isEnabled;
        copy.sceneservice = [self.sceneservice copyWithZone:zone];
        copy.deviceID = self.deviceID;
        copy.zoneid = self.zoneid;
    }
    
    return copy;
}


@end
