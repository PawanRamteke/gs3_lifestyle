//
//  DeviceListModel.h
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface DeviceListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *prevEDIPAddress;
@property (nonatomic, assign) double spaceID;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, assign) double owner;
@property (nonatomic, strong) NSString *gUID;
@property (nonatomic, strong) NSString *apPassword;
@property (nonatomic, strong) NSString *deviceSerialNo;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *imei;
@property (nonatomic, assign) BOOL deviceStatus;
@property (nonatomic, strong) NSString *activationDate;
@property (nonatomic, strong) NSString *prevAPIPAddress;
@property (nonatomic, strong) NSString *cipher;
@property (nonatomic, assign) double deviceID;
@property (nonatomic, strong) NSString *lastRechDate;
@property (nonatomic, strong) NSString *edIPAddress;
@property (nonatomic, strong) NSString *apIPAddress;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, assign) double roomID;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *deviceModel;
@property (nonatomic, strong) NSString *edPassword;
@property (nonatomic, strong) NSString *macAddress;
@property (nonatomic, strong) NSString *apSSID;
@property (nonatomic, strong) NSString *edSSID;
@property (nonatomic, assign) double zoneID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
