//
//  SpaceListModel.h
//
//  Created by Pawan Ramteke on 01/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SpaceListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double owner;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, assign) double spaceID;
@property (nonatomic, assign) BOOL isActiveSpace;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, assign) id image;
@property (nonatomic, strong) NSString *spaceName;
@property (nonatomic, strong) NSString *icon;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
