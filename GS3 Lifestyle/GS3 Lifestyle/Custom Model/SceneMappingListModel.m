//
//  SceneMappingListModel.m
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SceneMappingListModel.h"


NSString *const kSceneMappingListModelID = @"ID";
NSString *const kSceneMappingListModelServicePoint = @"servicePoint";
NSString *const kSceneMappingListModelDeviceID = @"deviceID";
NSString *const kSceneMappingListModelPointID = @"pointID";
NSString *const kSceneMappingListModelOutputStatus = @"outputStatus";
NSString *const kSceneMappingListModelSceneID = @"sceneID";


@interface SceneMappingListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SceneMappingListModel

@synthesize iDProperty = _iDProperty;
@synthesize servicePoint = _servicePoint;
@synthesize deviceID = _deviceID;
@synthesize pointID = _pointID;
@synthesize outputStatus = _outputStatus;
@synthesize sceneID = _sceneID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kSceneMappingListModelID fromDictionary:dict] doubleValue];
            self.servicePoint = [[self objectOrNilForKey:kSceneMappingListModelServicePoint fromDictionary:dict] doubleValue];
            self.deviceID = [[self objectOrNilForKey:kSceneMappingListModelDeviceID fromDictionary:dict] doubleValue];
            self.pointID = [[self objectOrNilForKey:kSceneMappingListModelPointID fromDictionary:dict] doubleValue];
            self.outputStatus = [[self objectOrNilForKey:kSceneMappingListModelOutputStatus fromDictionary:dict] doubleValue];
            self.sceneID = [[self objectOrNilForKey:kSceneMappingListModelSceneID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kSceneMappingListModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.servicePoint] forKey:kSceneMappingListModelServicePoint];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceID] forKey:kSceneMappingListModelDeviceID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pointID] forKey:kSceneMappingListModelPointID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.outputStatus] forKey:kSceneMappingListModelOutputStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sceneID] forKey:kSceneMappingListModelSceneID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kSceneMappingListModelID];
    self.servicePoint = [aDecoder decodeDoubleForKey:kSceneMappingListModelServicePoint];
    self.deviceID = [aDecoder decodeDoubleForKey:kSceneMappingListModelDeviceID];
    self.pointID = [aDecoder decodeDoubleForKey:kSceneMappingListModelPointID];
    self.outputStatus = [aDecoder decodeDoubleForKey:kSceneMappingListModelOutputStatus];
    self.sceneID = [aDecoder decodeDoubleForKey:kSceneMappingListModelSceneID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kSceneMappingListModelID];
    [aCoder encodeDouble:_servicePoint forKey:kSceneMappingListModelServicePoint];
    [aCoder encodeDouble:_deviceID forKey:kSceneMappingListModelDeviceID];
    [aCoder encodeDouble:_pointID forKey:kSceneMappingListModelPointID];
    [aCoder encodeDouble:_outputStatus forKey:kSceneMappingListModelOutputStatus];
    [aCoder encodeDouble:_sceneID forKey:kSceneMappingListModelSceneID];
}

- (id)copyWithZone:(NSZone *)zone
{
    SceneMappingListModel *copy = [[SceneMappingListModel alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.servicePoint = self.servicePoint;
        copy.deviceID = self.deviceID;
        copy.pointID = self.pointID;
        copy.outputStatus = self.outputStatus;
        copy.sceneID = self.sceneID;
    }
    
    return copy;
}


@end
