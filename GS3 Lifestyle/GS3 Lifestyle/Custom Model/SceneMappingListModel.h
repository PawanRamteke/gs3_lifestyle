//
//  SceneMappingListModel.h
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SceneMappingListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double servicePoint;
@property (nonatomic, assign) double deviceID;
@property (nonatomic, assign) double pointID;
@property (nonatomic, assign) double outputStatus;
@property (nonatomic, assign) double sceneID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
