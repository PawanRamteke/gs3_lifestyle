//
//  SceneListModel.h
//
//  Created by Pawan Ramteke on 02/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SceneListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *sceneName;
@property (nonatomic, assign) double roomid;
@property (nonatomic, assign) double owner;
@property (nonatomic, assign) double sceneID;
@property (nonatomic, strong) NSString *sceneType;
@property (nonatomic, assign) double spaceID;
@property (nonatomic, strong) NSString *switchinput;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) BOOL isEnabled;
@property (nonatomic, strong) NSString *sceneservice;
@property (nonatomic, assign) double deviceID;
@property (nonatomic, assign) double zoneid;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
