//
//  ModelParser.h
//  BusinessJodo
//
//  Created by Ketan Nandha on 03/03/17.
//  Copyright © 2017 KN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "General.h"
#import "DataModels.h"

@interface ModelParser : NSObject

+(id)parseModelWithAPIType:(NSString *)apiName responseData:(NSDictionary*)responseData;

@end
