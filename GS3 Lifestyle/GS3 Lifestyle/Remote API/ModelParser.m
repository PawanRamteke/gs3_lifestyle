//
//  ModelParser.m
//  BusinessJodo
//
//  Created by Ketan Nandha on 03/03/17.
//  Copyright © 2017 KN. All rights reserved.
//

#import "ModelParser.h"
#import "CoreDataHandler.h"
#import <CoreData/CoreData.h>

@implementation ModelParser

+(id)parseModelWithAPIType:(NSString *)apiName responseData:(NSDictionary*)responseData
{
    if ([apiName isEqualToString:LOGIN_URL]) {
       return [self parseLoginService:responseData];
    }
    
    if ([apiName containsString:GET_SPACE_URL]) {
        return [self parseGetSpaceService:responseData];
    }
    
    if ([apiName containsString:GET_DEVICE_LIST_URL]) {
        return [self parseGetDeviceListService:responseData];
    }
    
    if ([apiName containsString:GET_SCENE_LIST_URL]) {
        return [self parseGetSceneListService:responseData];
    }
    if ([apiName containsString:GET_FAVORITE_SCENE_LIST_URL]) {
        return [self parseGetFavouriteSceneListService:responseData];
    }
    
    if ([apiName containsString:GET_POINT_LIST_URL]) {
        return [self parseGetPointListService:responseData];
    }
    
    if ([apiName containsString:GET_FAVORITE_POINT_LIST_URL]) {
        return [self parseGetFavouritePointListService:responseData];
    }
    
    if ([apiName containsString:GET_ROOM_LIST_URL]) {
        return [self parseGetRoomListService:responseData];
    }
    
    if ([apiName containsString:GET_SCENE_MAPPING_URL]) {
        return [self parseGetSceneMappingListService:responseData];
    }
    
    return  nil;
}

+(id)parseLoginService:(NSDictionary *)responseData
{
    [Preferences setLoginResponse:responseData];
    LoginModel *model = [LoginModel modelObjectWithDictionary:responseData];
    return model;
}


+(id)parseGetSpaceService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        SpaceListModel *model = [SpaceListModel modelObjectWithDictionary:dict];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetDeviceListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        DeviceListModel *model = [DeviceListModel modelObjectWithDictionary:dict];

        [CoreDataHandler saveDeviceList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetSceneListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        SceneListModel *model = [SceneListModel modelObjectWithDictionary:dict];
        [CoreDataHandler saveSceneList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetFavouriteSceneListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        SceneListModel *model = [SceneListModel modelObjectWithDictionary:dict];
        [CoreDataHandler saveFavouriteSceneList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetPointListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        PointListModel *model = [PointListModel modelObjectWithDictionary:dict];
        
        [CoreDataHandler savePointList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetFavouritePointListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        PointListModel *model = [PointListModel modelObjectWithDictionary:dict];
        [CoreDataHandler saveFavouritePointList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}


+(id)parseGetRoomListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        RoomListModel *model = [RoomListModel modelObjectWithDictionary:dict];
        
        [CoreDataHandler saveRoomList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}

+(id)parseGetSceneMappingListService:(NSDictionary *)responseData
{
    NSArray *arrSpaceList = (NSArray *)responseData;
    NSMutableArray *arrToSend = [NSMutableArray new];
    
    for (NSDictionary *dict in arrSpaceList) {
        SceneMappingListModel *model = [SceneMappingListModel modelObjectWithDictionary:dict];
        
        [CoreDataHandler saveSceneMappingList:model];
        [arrToSend addObject:model];
    }
    return arrToSend;
}
@end
