//
//  CoreDataHandler.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 02/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "CoreDataHandler.h"

@implementation CoreDataHandler

#pragma mark - Device List by Space

+(void)saveDeviceList:(DeviceListModel *)model
{
    DeviceListbySpace *mod = nil;
    mod = [self getDeviceListRow:model.deviceID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([DeviceListbySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[DeviceListbySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.deviceId = model.deviceID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
    
}

+(DeviceListbySpace*)getDeviceListRow:(double)deviceId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND deviceId == %lf",spaceId,deviceId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[DeviceListbySpace class]];
    DeviceListbySpace *model = nil;
    if (result.count) {
       model = [result objectAtIndex:0];
    }
    return model;
}

+(NSArray *)getDeviceListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[DeviceListbySpace class]];
    
    for (DeviceListbySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        DeviceListModel *mod = [DeviceListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}

#pragma mark- Scene List by Space

+(void)saveSceneList:(SceneListModel *)model
{
    SceneListBySpace *mod = nil;
    mod = [self getSceneListRow:model.sceneID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([SceneListBySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[SceneListBySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.sceneId = model.sceneID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}
+(SceneListBySpace*)getSceneListRow:(double)sceneId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND sceneId == %lf",spaceId,sceneId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[SceneListBySpace class]];
    SceneListBySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}
+(NSArray *)getSceneListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[SceneListBySpace class]];
    
    for (SceneListBySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        SceneListModel *mod = [SceneListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}

#pragma mark- Favourite Scene List by Space

+(void)saveFavouriteSceneList:(SceneListModel *)model
{
    FavouriteSceneBySpace *mod = nil;
    mod = [self getFavouriteSceneListRow:model.sceneID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([FavouriteSceneBySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[FavouriteSceneBySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.sceneId = model.sceneID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}
+(FavouriteSceneBySpace*)getFavouriteSceneListRow:(double)sceneId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND sceneId == %lf",spaceId,sceneId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavouriteSceneBySpace class]];
    FavouriteSceneBySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}
+(NSArray *)getFavouriteSceneListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavouriteSceneBySpace class]];
    
    for (FavouriteSceneBySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        SceneListModel *mod = [SceneListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}

#pragma mark - Point List By Space

+(void)savePointList:(PointListModel *)model
{
    PointListbySpace *mod = nil;
    mod = [self getPointListRow:model.pointID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([PointListbySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[PointListbySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.pointId = model.pointID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}

+(PointListbySpace *)getPointListRow:(double)pointId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND pointId == %lf",spaceId,pointId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[PointListbySpace class]];
    PointListbySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(NSArray *)getPointListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[PointListbySpace class]];
    
    for (PointListbySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        PointListModel *mod = [PointListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}


#pragma mark - Favourite Point List by Space

+(void)saveFavouritePointList:(PointListModel *)model
{
    FavouritePointBySpace *mod = nil;
    mod = [self getFavouritePointListRow:model.pointID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([FavouritePointBySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[FavouritePointBySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.pointId = model.pointID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}

+(FavouritePointBySpace *)getFavouritePointListRow:(double)pointId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND pointId == %lf",spaceId,pointId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavouritePointBySpace class]];
    FavouritePointBySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(NSArray *)getFavouritePointListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavouritePointBySpace class]];
    
    for (FavouritePointBySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        PointListModel *mod = [PointListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}

#pragma mark - Room List by Space

+(void)saveRoomList:(RoomListModel *)model
{
    RoomListBySpace *mod = nil;
    mod = [self getRoomListRow:model.roomID];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([RoomListBySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[RoomListBySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.roomId = model.roomID;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}

+(RoomListBySpace *)getRoomListRow:(double)roomId
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND roomId == %lf",spaceId,roomId];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[RoomListBySpace class]];
    RoomListBySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(NSArray *)getRoomListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[RoomListBySpace class]];
    
    for (RoomListBySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        RoomListModel *mod = [RoomListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}

#pragma mark - Scene Mapping List By Space

+(void)saveSceneMappingList:(SceneMappingListModel *)model
{
    SceneMappingListBySpace *mod = nil;
    mod = [self getSceneMappingListRow:model.iDProperty];
    if (mod == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([SceneMappingListBySpace class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        mod = [[SceneMappingListBySpace alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    
    NSData *responseData = [NSKeyedArchiver archivedDataWithRootObject:[model dictionaryRepresentation]];
    
    
    if (responseData != nil) {
        mod.responseData = responseData;
    }
    mod.iDProperty = model.iDProperty;
    mod.spaceId = VIEWMANAGER.spaceId;
    
    [self saveAndUpdate];
}

+(SceneMappingListBySpace *)getSceneMappingListRow:(double)iDProperty
{
    double spaceId = VIEWMANAGER.spaceId;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf AND iDProperty == %lf",spaceId,iDProperty];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[SceneMappingListBySpace class]];
    SceneMappingListBySpace *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(NSArray *)getSceneMappingListFromCoreData
{
    double spaceId = VIEWMANAGER.spaceId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spaceId == %lf",spaceId];
    
    NSMutableArray *dataToSend = [NSMutableArray new];
    NSArray *result = [self findDataByPredicate:predicate withClass:[SceneMappingListBySpace class]];
    
    for (SceneMappingListBySpace *model in result) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:model.responseData];
        SceneMappingListModel *mod = [SceneMappingListModel modelObjectWithDictionary:dict];
        [dataToSend addObject:mod];
    }
    return dataToSend;
}



#pragma mark-

+(NSArray *)findDataByPredicate:(NSPredicate *)predicate withClass:(Class)class
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    req.predicate = predicate;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    req.entity = entityDescription;
    NSArray *result = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:req error:nil];
    return result;
}


+(void)saveAndUpdate
{
    NSError *error = nil;
    [VIEWMANAGER.appManagedObjectContext save:&error];
}

@end
