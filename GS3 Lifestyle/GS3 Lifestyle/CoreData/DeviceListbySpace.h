//
//  DeviceListbySpace+CoreDataClass.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 02/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceListbySpace : NSManagedObject
+(void)saveDeviceList:(DeviceListModel *)model;
@end

NS_ASSUME_NONNULL_END

#import "DeviceListbySpace+CoreDataProperties.h"
