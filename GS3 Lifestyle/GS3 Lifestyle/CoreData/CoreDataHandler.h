//
//  CoreDataHandler.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 02/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DeviceListbySpace+CoreDataProperties.h"
#import "SceneListBySpace+CoreDataProperties.h"
#import "FavouriteSceneBySpace+CoreDataProperties.h"
#import "PointListBySpace+CoreDataProperties.h"
#import "FavouritePointBySpace+CoreDataProperties.h"
#import "RoomListBySpace+CoreDataProperties.h"
#import "SceneMappingListBySpace+CoreDataClass.h"

@interface CoreDataHandler : NSObject
//Device List By Space
+(void)saveDeviceList:(DeviceListModel *)model;
+(DeviceListbySpace*)getDeviceListRow:(double)deviceId;
+(NSArray *)getDeviceListFromCoreData;

//Scene List By Space
+(void)saveSceneList:(SceneListModel *)model;
+(SceneListBySpace*)getSceneListRow:(double)sceneId;
+(NSArray *)getSceneListFromCoreData;

//Favourite Scene List By Space
+(void)saveFavouriteSceneList:(SceneListModel *)model;
+(FavouriteSceneBySpace*)getFavouriteSceneListRow:(double)sceneId;
+(NSArray *)getFavouriteSceneListFromCoreData;

//Point List By Space
+(void)savePointList:(PointListModel *)model;
+(PointListbySpace *)getPointListRow:(double)pointId;
+(NSArray *)getPointListFromCoreData;

//Favourite Point List By Space
+(void)saveFavouritePointList:(PointListModel *)model;
+(PointListbySpace *)getFavouritePointListRow:(double)pointId;
+(NSArray *)getFavouritePointListFromCoreData;

//Room List by Space
+(void)saveRoomList:(RoomListModel *)model;
+(RoomListBySpace *)getRoomListRow:(double)roomId;
+(NSArray *)getRoomListFromCoreData;

//Scene Mapping List By Space
+(void)saveSceneMappingList:(SceneMappingListModel *)model;
+(SceneMappingListBySpace *)getSceneMappingListRow:(double)iDProperty;
+(NSArray *)getSceneMappingListFromCoreData;

@end
