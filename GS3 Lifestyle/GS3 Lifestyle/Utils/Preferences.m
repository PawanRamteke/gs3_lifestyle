//
//  Preferences.m
//  SCB
//
//  Created by You on 14/07/17.
//  Copyright © 2017 You. All rights reserved.
//

#import "Preferences.h"

@implementation Preferences
+(void)setLoginResponse:(NSDictionary*)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"getLoginResponse"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSDictionary*)getLoginResponse
{
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] valueForKey:@"getLoginResponse"];
    
    return value;
}

@end
