//
//  UIFont+App.h
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (App)
+(UIFont *)appRegularFont:(CGFloat)size;
+(UIFont *)appBoldFont:(CGFloat)size;

@end
