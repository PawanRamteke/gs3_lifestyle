//
//  UIFont+App.m
//  GS3 Lifestyle
//
//  Created by Pawan Ramteke on 31/01/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "UIFont+App.h"

@implementation UIFont (App)
+(UIFont *)appRegularFont:(CGFloat)size
{
    return FONT_LIGHT(size);
}

+(UIFont *)appBoldFont:(CGFloat)size
{
    return FONT_BOLD(size);
}
@end
