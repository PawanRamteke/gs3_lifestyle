//
//  UIColor+App.m
//  WhatsApp Video Status
//
//  Created by Pawan Ramteke on 24/12/17.
//  Copyright © 2017 Pawan Ramteke. All rights reserved.
//

#import "UIColor+App.h"

@implementation UIColor (App)


+(UIColor *)appYellowColor
{
    return RGB(243, 167, 57);
}

+(UIColor *)darkYellowColor
{
    return RGB(255, 147, 0);
}

+(UIColor *)transperentColor
{
    return RGB_Alpha(0, 0, 0, 0.5);
}
@end
