//
//  UIColor+App.h
//  WhatsApp Video Status
//
//  Created by Pawan Ramteke on 24/12/17.
//  Copyright © 2017 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (App)
+(UIColor *)appYellowColor;
+(UIColor *)darkYellowColor;
+(UIColor *)transperentColor;

@end
