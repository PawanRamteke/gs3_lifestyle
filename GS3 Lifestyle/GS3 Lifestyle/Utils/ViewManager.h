//
//  ViewManager.h
//  PampasGroup
//
//  Created by You on 11/04/17.
//  Copyright © 2017 You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginModel.h"

@interface ViewManager : NSObject

@property (nonatomic,assign)BOOL isNetworkAvailable;
@property (nonatomic,strong)LoginModel *currentUser;
@property (nonatomic,assign)double spaceId;
@property (nonatomic,strong)NSArray *arrFavouriteScene;
@property (nonatomic,strong)NSArray *arrRoomList;
@property(nonatomic,strong) NSManagedObjectContext *appManagedObjectContext;


+ (ViewManager *) viewManager;
- (CGFloat)getFlexibleHeight:(CGFloat)height;
- (UIViewController *) topMostController;

-(void)showAcytivityIndicator;
-(void)showAcytivityIndicator:(NSString*)title;
-(void)hideAcytivityIndicator;

-(BOOL) validateEmail: (NSString *) candidate;
-(NSString *)deviceID;
-(NSString *)stringFromDate:(NSDate *)date format:(NSString *)format;
-(NSString *)getAppDateFormat:(NSString*)dateString;
-(NSString *)getAppDateFormatShort:(NSString*)dateString;
-(void)popToSpecificController:(Class)contollerClass;
-(UIWindow*)getAppWindow;

-(BOOL)isUserLoggedIn;

@end
