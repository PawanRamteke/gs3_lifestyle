//
//  General.h
//  WhatsApp Video Status
//
//  Created by Pawan Ramteke on 24/12/17.
//  Copyright © 2017 Pawan Ramteke. All rights reserved.
//

#ifndef General_h
#define General_h

#define RGB(r,g,b)          [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1.0]
#define RGB_Alpha(r,g,b,a)  [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:a]

#define SCREEN_WIDTH        ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT       ([[UIScreen mainScreen] bounds].size.height)

#define FONT_LIGHT(size1) [UIFont fontWithName:@"ArialMT" size:size1]
#define FONT_BOLD(size1) [UIFont fontWithName:@"Arial-BoldMT" size:size1]

//#define VIEWMANAGER         [ViewManager getSharedInstance]
#define REMOTE_API      [[RemoteAPI alloc]init]

#define BASE_URL        @"http://8.26.21.192:85/"
#define LOGIN_URL   @"Login"
#define GET_SPACE_URL   @"Getgs3_spaceList"
#define GET_DEVICE_LIST_URL   @"GetDeviceListbySpace"
#define GET_SCENE_LIST_URL     @"GetScenebySpace"
#define GET_FAVORITE_SCENE_LIST_URL  @"GetFavouriteScenebySpace"
#define GET_POINT_LIST_URL      @"GetPointListbySpace"
#define GET_FAVORITE_POINT_LIST_URL @"GetFavouritePointListbySpace"
#define GET_ROOM_LIST_URL           @"GetRoombySpace"
#define GET_SCENE_MAPPING_URL       @"GetSceneMappingList"

#define VIEWMANAGER         [ViewManager viewManager]

#endif /* General_h */
