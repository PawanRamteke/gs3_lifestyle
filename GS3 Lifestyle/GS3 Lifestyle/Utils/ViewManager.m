//
//  ViewManager.m
//  PampasGroup
//
//  Created by You on 11/04/17.
//  Copyright © 2017 You. All rights reserved.
//

#import "ViewManager.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"


@implementation ViewManager

+ (ViewManager *)viewManager
{
    static ViewManager *viewManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        viewManager = [[self alloc] init];
    });
    return viewManager;
}

- (id)init
{
    if (self = [super init])
    {
    }
    return self;
}

-(UIWindow*)getAppWindow
{
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return delegate.window;
}

- (CGFloat)getFlexibleHeight:(CGFloat)height
{
    CGFloat flexibleheight;
    flexibleheight = height * (SCREEN_WIDTH/375);
    return flexibleheight;
}

- (UIViewController *) topMostController
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (true)
    {
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else {
            break;
        }
    }
    
    return topViewController;
}

-(void)showAcytivityIndicator
{
    NSLog(@"Show======>");
    
    // [SVProgressHUD sharedView].strokeColorForApp = [UIColor whiteColor];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setRingThickness:2];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [SVProgressHUD show];
    
    
    //self.indicatorCount++;
    
}
-(void)showAcytivityIndicator:(NSString*)title
{
    //  [SVProgressHUD sharedView].strokeColorForApp = [UIColor whiteColor];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setRingThickness:2];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    [SVProgressHUD showWithStatus:title];
    
    //self.indicatorCount++;
}
-(void)hideAcytivityIndicator
{
    NSLog(@"Hide======>");
    
    [SVProgressHUD dismiss];
}

-(NSString *)getReplacingURLString:(NSString *)url
{
    //return [url stringByReplacingOccurrencesOfString:@"localhost~" withString:BASE_URL];
    NSString *str = [NSString stringWithFormat:@"http://%@",[url stringByReplacingOccurrencesOfString:@"~" withString:@""]];
    return str;
}


-(BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];    //  return 0;
    return [emailTest evaluateWithObject:candidate];
}

-(NSString *)deviceID
{
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    return  currentDeviceId;
}

-(NSString *)stringFromDate:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:format];
    NSString *myFormattedDate = [dateFormatter stringFromDate:date];
    return myFormattedDate;
}


-(NSString *)getAppDateFormat:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    NSDate *dateOld = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"dd-MMM-yy hh:mm a"];

    NSString *myFormattedDate = [dateFormatter stringFromDate:dateOld];
    return myFormattedDate;
}

-(NSString *)getAppDateFormatShort:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    NSDate *dateOld = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"dd-MM-yy"];
    
    NSString *myFormattedDate = [dateFormatter stringFromDate:dateOld];
    return myFormattedDate;
}

-(void)popToSpecificController:(Class)contollerClass
{
    NSArray *arr = [self topMostController].navigationController.viewControllers;
    //NSLog(@"arr = %@",arr);
    
    for (UIViewController *controller in [self topMostController].navigationController.viewControllers)
    {
        if ([controller isKindOfClass:contollerClass])
        {
            //Do not forget to import AnOldViewController.h
            [[self topMostController].navigationController popToViewController:controller
                                                                      animated:YES];
            break;
        }
    }
}

-(BOOL)isUserLoggedIn
{
    if (self.currentUser.userId != nil) {
        return YES;
    }
    return NO;
}

@end
