//
//  Preferences.h
//  SCB
//
//  Created by You on 14/07/17.
//  Copyright © 2017 You. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preferences : NSObject
+(void)setLoginResponse:(NSDictionary*)value;
+(NSDictionary*)getLoginResponse;

@end
